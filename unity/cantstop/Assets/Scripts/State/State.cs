using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace State {
    public abstract class State : ScriptableObject {
        public bool isFinished { get; protected set; }
        [HideInInspector] public GameObject actor;

        public virtual void init() {}
        public abstract void step();
    }
}
