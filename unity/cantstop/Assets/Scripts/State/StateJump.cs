using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace State {
    [CreateAssetMenu]
    public class StateJump : State {

        [Header("Actual State 2")]
        float x, y;
        float iDuration = 100.0f;
        float t0;
        float t1;
        float ddz = 0;

        public override void init() {
            t0 = Time.time;
            t1 = t0;
        }

        public override void step() {
            if (isFinished) {
                return;
            }
            float t = Time.time;
            if (t - t0 >= iDuration) {
                isFinished = true;
                return;
            }

            // dz = h + xtan(α) - gx²/2V₀²cos²(α)

            // s = s0 + v * t - g/2*t*t
            float k = 20.0f;
            float v0 = 0.1f * k;
            float g = -0.098f * k;

float tt0 = t - t0;
float t1t0 = t1 - t0;

            float dz = (v0 * tt0 + g/2*tt0*tt0) - (v0 * t1t0 + g/2*t1t0*t1t0);

            ddz += dz;
            if (ddz < 0.0f) {
                dz -= ddz;
                isFinished = true;
            }

            // 0.01f 
            actor.transform.position = actor.transform.position + new Vector3(0, dz, 0);

            t1 = t;
        }
    }
}