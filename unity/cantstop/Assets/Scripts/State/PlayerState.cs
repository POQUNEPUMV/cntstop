using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace State {
    public class PlayerState0 : MonoBehaviour {
        [Header("Actual State")]
        public State stateCurrent;
            public void Start()
    {
    }
    public void Update() {}

    public void stateSet(State stateNew) {
        stateCurrent = Instantiate(stateNew);
        stateCurrent.actor = gameObject;
        stateCurrent.init();
    }
    }

public class PlayerState : PlayerState0
{
    [Header("Initial Params")]
    public int lives = 1;

    public State stateInit;
    public State stateJump;

    // public State.State StateFall;
    // public State.State StateStill;

    public Animator animator;

    [Header("Actual State1")]
    public GameObject sphereGuide;


    // Start is called before the first frame update
    void Start()
    {
        base.Start();

        Debug.Log("PlayerState::Start");
        stateInit = new StateInit();
        stateJump = new StateJump();
        stateSet(stateInit);
    }

    // Update is called once per frame
    void Update()
    {

        float gx = Mathf.Sin(Time.time * 5.0f);
        float gz = Mathf.Cos(Time.time * 5.0f);
        float r = 0.5f + 0.1f;
        sphereGuide.transform.localPosition = new Vector3(r*gx,r*gz,0);


        if (stateCurrent.isFinished == false) {
            stateCurrent.step();
            return;
        }

        if (Input.GetKeyDown(KeyCode.Space)) {
            stateSet(stateJump);
            return;
        }
        if (stateCurrent is StateJump) {
            stateSet(stateInit);
        }
    }


}

}

