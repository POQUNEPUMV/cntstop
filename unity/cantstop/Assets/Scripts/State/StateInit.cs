using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace State {
    [CreateAssetMenu]
    public class StateInit : State {
        public override void init() {
            actor.transform.position = new Vector3(0, 0, 0);

            isFinished = true;
        }

        public override void step() {
        }
    }
}