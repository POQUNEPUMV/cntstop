using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class testMove : MonoBehaviour
{
    float y;
     enum MoveState { Stay, Jump }
     MoveState moveState = MoveState.Stay;
    double moveStateTime = 0.0;
    double duration = 0.5;

    double f(float t) {
        return Mathf.Sin(t * 3.1415926f / 360.0f);
    }

    // Start is called before the first frame update
    void Start() {
    }

    // Update is called once per frame
    void Update()
    {
        


        switch (moveState) {
            case MoveState.Stay:
                    if (Input.GetKey(KeyCode.Space)) {
            moveState = MoveState.Jump;
            moveStateTime = Time.time;
        } 
            break;
            
            case MoveState.Jump:
            double dt = Time.time - moveStateTime;
            if (dt > duration) {
                moveState = MoveState.Stay;
                break;
            }
            double ff = (dt / duration);
            double dd = f((float)ff) * 0.1f;
            transform.Translate(0, (float)dd, 0);
            break;
            default:
            break;
        }
        
    }
}
